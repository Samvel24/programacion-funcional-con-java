
package Funciones;

import java.util.function.Function;
import java.util.function.Predicate;

public class MathFunctions {
    public static void main(String[] args) {
        /*
        Function representa una función que acepta un argumento y produce un resultado.
        */ 
        // Una forma de definir funciones por medio de la interface Function es la siguiente:
        Function<Integer, Integer> squareFunction = new Function<Integer, Integer>() {
            @Override
            public Integer apply(Integer x) {
                return x * x;
            }
        };
        
        System.out.println(squareFunction.apply(5));
        System.out.println(squareFunction.apply(25));
        /* 
        Las llamadas a la función apply funcionan de manera similar a la creación y llamda de un 
        método común de Java pero esta vez estamos usando genéricos de este lenguaje.
        */
        System.out.println(square(5));
        
        // Otra manera de definir funciones por medio de la interface Function es usando expresiones lambda
        Function<Integer, Boolean> esImpar = x -> x % 2 != 0;
        
        /* Predicate es una función que trabaja sobre un tipo y devuelve un boolean, es decir,
        evalua o testea si algo es válido.
        */
        int num = 4;
        Predicate<Integer> esPar = x -> x % 2 == 0;
        System.out.println("¿" + num + " es par? " + esPar.test(num));
       
        // Ejemplo pŕactico del uso de la interface Predicate
        Predicate <Student> estaAprobado = student -> student.getCalificacion() >= 6.0;
        Student samuel = new Student(5.9);
        System.out.println("¿El estudiante ha aprobado? " + estaAprobado.test(samuel));
    }
    
    static int square(int x) {
        return x * x;
    }
    
    static class Student {
        private double calificacion;

        public Student(double calificacion) {
            this.calificacion = calificacion;
        }
        
        public double getCalificacion() {
            return calificacion;
        }
    }
}
