
package Mutable;

import java.util.LinkedList;
import java.util.List;

public class Outsider {
    public static void main(String[] args) {
        List <String> sierEmail = new LinkedList<>();
	sierEmail.add("sier@sier.com"); // Correos de una persona

	MutablePerson sier = new MutablePerson(); // Creamos un objeto Persona
        // Asignamos datos al objeto Persona
	sier.setEmails(sierEmail);
	sier.setFirstName("Samuel");
	sier.setFirstName("Sergio");

	// Veamos un poco los peligros de una clase mutable
	System.out.println(sier);
        /* 
        Llamamos a una función impura (badFunction) que toma al objeto Persona, es una 
        función que no sabemos que hace, tal vez es de una libreria, de un 
        framework, etc., Al imprimir este objeto por pantalla dos veces, deberíamos
        observar que los resultados son iguales sin embargo vemos que a pesar de que se 
        establecio una cadena para un correo, el llamado a esta función hizo 
        modificaciones sobre el objeto en cuestión, esas modificaciones son resultado
        de que la clase de este objeto sea mutable, tener una clase mutable genera este
        tipo de problemas.
        */
	badFunction(sier);
	System.out.println(sier);
        
        /*
        Del ejemplo anterior (llamada a badFunction), podemos considerar que el error
        estaba en tener acceso a la lista de correos del objeto.
        Ahora generamos una nueva clase (MutablePerson_2) que ya no va a ser mutable 
        y que ya no va a ser perjudicada por el hacker que nos cambia el correo.
        En la clase MutablePerson_2 se quitó la parte en la cual se agregaba un setter
        para los correos y por tanto ya no se podrá invocar tal método.
        También se modifico el constructor, en este nos van a tener que proveer de los
        datos. Esto debería arreglar el problema que estamos abordando.
        */
        System.out.println("///////////////////////////////");

        MutablePerson_2 sinuhe = new MutablePerson_2(sierEmail);
        System.out.println(sinuhe);
        /*
        Al ver los resultados de llamar a la función otherBadFunction() podemos
        identificar que nos siguen hackeando los datos, resulta que los datos
        mutables siguen siendo perjudiciales, entonces no basta con solo configurar
        el constructor para recibir los datos y esconder los setters, tenemos que
        hacer algo más.
        ¿Qué es lo que están haciendo las funciones que estamos invocando?
        Alteran nuestros datos porque son mutables
        */
        otherBadFunction(sinuhe);
        System.out.println(sinuhe);

        /*
        Para seguir abordando el problema en cuestión se propone hacer que la lista
        de correos sea final, es decir que sea una constante y por tanto será un dato
        que no podremos cambiar una vez que lo inicialicemos.
        */
        System.out.println("///////////////////////////////");
        
        MutablePerson_3 sinuhe_3 = new MutablePerson_3(sierEmail);
        System.out.println(sinuhe_3);
        /*
        Al ejecutar este código de nuevo podemos observar que el problema continua
        e incluso nos están agregando correos de spam entonces no está siendo efectivo
        agregar la palabra reservada final.
        */
        otherBadFunctionPart3(sinuhe_3);
        System.out.println(sinuhe_3);

        /*
        ¿Por qué no podemos arreglar el problema en cuestión?
        Antes de contestar esta pregunta primero creamos una nueva clase 
        (MutablePerson_4), a través de esta clase no podrán hackearnos, sin embargo al 
        observar en su interior vemos que alguien tomo esta clase y le heredo la clase 
        MutablePerson_3 y al hacer esto nos hacen creer que se está comportando como 
        la clase original.
        */
        System.out.println("///////////////////////////////");

        /*
        En la siguiente instrucción tenemos a MutablePerson_3 en la que creemos que 
        arreglamos la mutabilidad pero alguien generó una nueva clase a partir de
        MutablePerson_4 y resulta que esa clase hace cosas maliciosas por dentro.
        
        Las cosas maliciosas que hace es que evita que se vea reflejado el email
        correcto (en el método getEmails() y al usar la lista spammyEmails) y por 
        tanto se están devolviendo nuevos emails.
        
        Con todo esto podemos ver que nuestra clase es mutable por donde le busquemos
        */
        MutablePerson_3 sinuhe_4 = new MutablePerson_4(sierEmail);
        System.out.println(sinuhe_4);
    }
    
    /*
     * Este metodo modifica la lista mediante un setter.
     * Tener el setter es peligroso…
    */
    static void badFunction(MutablePerson person) {
        List<String> emails = new LinkedList<>();
        emails.add("imnotevil@mail.com");

        person.setEmails(emails);
    }
    
    /**
     * Este metodo toma el objeto devuelto por el getter… pero el
     * objeto es mutable, asi que podemos modificarlo sin restricciones…
     */
    static void otherBadFunction(MutablePerson_2 person) {
        List<String> emails = person.getEmails();
        emails.clear();

        emails.add("imnotevil@mail.com");
    }

    static void otherBadFunctionPart3(MutablePerson_3 person) {
        List<String> spammyEmails = new LinkedList<>();
        spammyEmails.add("tubanco@mibanco.banco.com");
        spammyEmails.add("cheapfoods@blackmarket.com");

        List<String> emails = person.getEmails();
        emails.clear();

        emails.add("imnotevil@mail.com");
        emails.addAll(spammyEmails);
    }
}
